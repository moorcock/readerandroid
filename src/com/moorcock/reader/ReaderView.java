package com.moorcock.reader;

import android.content.Context;
import android.graphics.*;
import android.view.View;

/**
 * Created by IntelliJ IDEA.
 * User: moorcock
 * Date: 13.11.11
 * Time: 16:16
 * To change this template use File | Settings | File Templates.
 */
public class ReaderView extends View {
	private Paint mPaint;

	private String testString;
	private String allText;

	public ReaderView(Context context) {
		super(context);
		setFocusable(true);

		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setTextSize(18);
		mPaint.setTypeface(Typeface.SERIF);
	}

	public void setTestString(String string) {
		testString = string;
	}

	public void setAllText(String text) {
		allText = text;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawColor(Color.WHITE);

		Paint p = mPaint;
		p.setTextAlign(Paint.Align.LEFT);

		int lineInterval = 6;
		int paddingTop = 20;
		int paddingBottom = 20;
		int paddingLeft = 10;
		int paddingRight = 10;

		int maxWidth = canvas.getWidth() - paddingLeft - paddingRight;
		int maxHeight = canvas.getHeight() - paddingTop - paddingBottom;

		String current = allText;
		int charsInString = 0;
		Rect textRect = new Rect();
		int textHeight = paddingTop;

		while (current.length() > 0 && textHeight < maxHeight) {
			p.getTextBounds(current, 0, current.length(), textRect);
			charsInString = p.breakText(current, true, maxWidth, null);
			canvas.drawText(current.substring(0, charsInString), paddingLeft, textHeight, p);
			textHeight += (textRect.height() + lineInterval);
			current = current.substring(charsInString).trim();
		}
	}
}
