package com.example;

import android.app.Activity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import com.moorcock.reader.ReaderView;

public class MyActivity extends Activity {
	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		ReaderView view = new ReaderView(this);
		view.setTestString(getResources().getString(R.string.test_string));
		view.setAllText(getResources().getString(R.string.all_text));

		FrameLayout layout = new FrameLayout(this);
		layout.addView(view);

		setContentView(layout);
	}
}